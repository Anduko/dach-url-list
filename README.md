# DACH Dachverband

* Gitlab Page: https://dachverband.gitlab.io/dach-url-list/ 
  * Instant View Link: https://t.me/iv?url=https%3A%2F%2Fdachverband.gitlab.io%2Fdach-url-list%2F&rhash=ea1b0cacd3a4cf
* JSON API: https://dachverband.gitlab.io/dach-url-list/api.json
## Gruppen verwalten

### Neue Gruppe
1) Neuen Branch anlegen (`master` als Grundlage)
2) Neue JSON Datei im Ordner `groups` anlegen
      * JSON muss nach diesem Schema erstellt werden: `{"name":"Gruppenname", "url":"t.me URL zur Gruppe", "tags" : ["liste", "von", "tags"]}`
          * `tags` ist dabei optional.
3) Merge Request vom neuen Branch in Master erstellen
4) Wenn MR akzeptiert wird, erscheint die Gruppe nach kurzer Zeit in der Liste.

### Gruppe(n) bearbeiten
1) Neuen Branch anlegen (`master` als Grundlage)
2) JSON Datei der zu bearbeitenden Gruppe(n) anpassen
3) Merge Request vom neuen Branch in Master erstellen
4) Wenn MR akzeptiert wird, erscheinen die Änderungen nach kurzer Zeit in der Liste.



